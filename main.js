function checkValidation()
{
    var strName = document.getElementById('txtName').value;
    var storeName = localStorage.setItem("storeName", strName);

    var rdGender = document.getElementById('gender').value;
    var storeGender = localStorage.setItem("storeGender", rdGender);

    var strDate = document.getElementById('dateBirth').value;
    var storeDate = localStorage.setItem("storeDate", strDate);

    //validation for email
    var strEmail = document.getElementById('txtEmail').value;
    var checkEmail = /^([a-zA-Z\.]+)@([gmail/yudiz]+)\.([com]{3})$/;
    
    var regexEmail = new RegExp(checkEmail);
    if(regexEmail.test(strEmail))
    {
        document.getElementById('emailValidate').innerHTML = '';
        var storeEmail = localStorage.setItem("storeEmail", strEmail);
    }
    else
    {
        document.getElementById('emailValidate').innerHTML = "Please enter a valid email id.";
        return false;
    }

    // //validation for mobile
    var strMobile = document.getElementById('txtMobile').value;
    var checkMobile = /^(\+[91]{2})([789][0-9]{9})$/;
    
    var regexMobile = new RegExp(checkMobile);
    if(regexMobile.test(strMobile)){
        document.getElementById('mobileValidate').innerHTML = '';
        var storeMobile = localStorage.setItem("storeMobile", strMobile);
        // console.log(storeMobile);
    }
    else{
        document.getElementById('mobileValidate').innerHTML = "Please enter a valid mobile number.(i.e: +91 9998887770)";
        return false;
    }

    // //validation for password
    var strPassword = document.getElementById('txtPassword').value;
    var checkPassword = /^([A-Z]{1})([a-z]+)([!@#$&*]{1})([0-9]{4})$/;
    /* 
        [A-Z]{1}) should contain atleast one uppercase letters.
        ([a-z]+) should contain lowercase letters.
        ([!@#$&*]{1}) should contain one special character.
        ([0-9]{4}) should contain four digits.
    */
    
    var regexPassword = new RegExp(checkPassword);
    if(regexPassword.test(strPassword)){
        document.getElementById('passwordValidate').innerHTML = '';
        var storePassword = localStorage.setItem("storePassword", strPassword);
        //console.log(storePassword);
    }
    else{
        document.getElementById('passwordValidate').innerHTML = "Please enter a strong password.<br>It should contain atleast one uppercase letters,<br>lowercase letters,<br>one special character,<br>and four digits";
        return false;
    }
}

//const aUser = [];
function checkValidateUser(){


    let getEmail = localStorage.getItem('storeEmail');
    let getPassword = localStorage.getItem('storePassword');
    //alert(getEmail);

    var email = document.getElementById('txtEmailLogin').value;
    var password = document.getElementById('txtPasswordLogin').value;

    // Check browser support

    // Validation For Email Login
    if (email === getEmail) {

        if (password === getPassword) {

            //alert("Welcome TO Yudiz Solution!!!" + getEmail);
            window.open('profile.html');
        }
        else {
            alert("Password Is Incorrect");
        }
        //console.log(getEmail);
    }
    else {
        alert("Invalid Email Or Password.");
    }

}

function clearStorage(){
    localStorage.clear();
    window.open('index.html');
}

//edit profile function
function editProfile(){
    
    alert('Editing Profile');
    var getNameData = localStorage.getItem('storeName');
    document.getElementById("txtNameData").value = getNameData;
    
    var getEmailData = localStorage.getItem('storeEmail');
    document.getElementById("txtEmailData").value = getEmailData;

    var getMobileData = localStorage.getItem('storeMobile');
    document.getElementById("txtMobileData").value = getMobileData;
}

//update button
function update(){
    //console.log(getNameData);
    
    if(document.getElementById("txtNameData").value != getNameData || document.getElementById("txtEmailData").value != getEmailData || document.getElementById("txtMobileData").value != getMobileData){
        var strNewName = document.getElementById('txtNameData').value;
        var getNameData = localStorage.setItem('storeName', strNewName);
        
        var strNewEmail = document.getElementById('txtEmailData').value;
        var getEmailData = localStorage.setItem('storeEmail', strNewEmail);
        
        var strNewMobile = document.getElementById('txtMobileData').value;
        var getMobileData = localStorage.setItem('storeMobile', strNewMobile);
        //console.log(getNameData);
        alert('Data Updated Successfully.');
        window.open('profile.html');
    }
}

//cancel button
function cancel(){
    confirm('Are you sure you wanna go back to profile?');
    window.open('profile.html');
}

//get data in the profile page
function getData(){
    var getNameLogo = localStorage.getItem('storeName');
    console.log(getNameLogo);
    document.getElementById('resultNameLogo').innerHTML = 'Hi! ' + getNameLogo;

    var getName = localStorage.getItem('storeName');
    console.log(getName);
    document.getElementById('resultName').innerHTML = getName;

    var getPassword = localStorage.getItem('storePassword');
    console.log(getPassword);
    document.getElementById('resultPassword').innerHTML = getPassword;

    var getEmail = localStorage.getItem('storeEmail');
    console.log(getEmail);
    document.getElementById('resultEmail').innerHTML = getEmail;

    var getMobile = localStorage.getItem('storeMobile');
    console.log(getMobile);
    document.getElementById('resultMobile').innerHTML = getMobile;

    var getGender = localStorage.getItem('storeGender');
    console.log(getGender);
    document.getElementById('resultGender').innerHTML = getGender;

    var getDate = localStorage.getItem('storeDate');
    console.log(getDate);
    document.getElementById('resultDate').innerHTML = getDate;
}

//change Password
function changePassword(){
    var getOldPassword = localStorage.getItem('storePassword');

    var strOldPass = document.getElementById('txtOldPassword').value;

    if(strOldPass === getOldPassword){
        var strNewPass = document.getElementById('txtNewPassword').value;
        var setNewPass = localStorage.setItem('storePassword', strNewPass);

        alert('Password Changed Successfully.');
        window.open('profile.html');
    }
    else{
        alert('Your old password does not matches...');
    }

}

//current time and weather
function currentTime() {
    setInterval(() => {
        const date = new Date();
        document.getElementById("getCurrentTime").innerHTML = date.toLocaleTimeString();
    }, 1000);

    fetch('https://api.giphy.com/v1/gifs/search?q=funny+cat&api_key=AdqSuQfxgFIDAcPaU5om8iv86DBEY3FV')
    .then(response => response.json())
    .then(json => {
        console.log(json.data)
        count = 0;

        setInterval(() => {
            document.getElementById("image").src = json.data[count].images.original.url;
            count++;
        }, 1000 * 120);
    })  
    
    navigator.geolocation.getCurrentPosition((position) => {
        let lat = position.coords.latitude;
        let lon = position.coords.longitude;

        let api = "https://api.openweathermap.org/data/2.5/weather";
        let apiKey = "f146799a557e8ab658304c1b30cc3cfd";

        url = api + "?lat=" + lat + "&lon=" + lon + "&appid=" + apiKey + "&units=imperial";
        
        //console.log(url);
        
        fetch(url) //api for the get request
        .then((response) => response.json())
        .then((data) => {
            document.getElementById("temperature").innerHTML = "Temperature : " + data.main.temp + "f°";

            document.getElementById("icon").src = data.weather[0].icon + "png";

            document.getElementById("location").innerHTML = "Location : " + data.name + " <p style='color: red; margin: 0; padding: 0;'>(" + lat + "°, " + lon + "°)</p>";
            document.getElementById("description").innerHTML = "Weather : <p style='color: green; margin: 0; padding: 0;'>" + data.weather[0].main + "</p>";
        });
    });   
}

//add task to todo list
function addTask(){
    //createing li element dynamic 
    var li = document.createElement("li");

    var getInput = document.getElementById("addTodo").value;
    var todoList = document.createTextNode(getInput);

    li.appendChild(todoList);

    if(getInput === ''){
        alert("Write something to add todo task!");
    } 
    else{
        document.getElementById("myTodo").appendChild(li);
    }

    document.getElementById("addTodo").value = "";
}